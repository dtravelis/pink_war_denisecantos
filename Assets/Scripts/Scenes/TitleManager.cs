﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{

    public AudioSource buttonA;

    public void PulsaStart()
    {
        buttonA.Play();
        SceneManager.LoadScene("GameplayScreen");
    }

    public void PulsaCredits()
    {
        buttonA.Play();
        SceneManager.LoadScene("CreditsScreen");
    }

    public void PulsaExit()
    {
        buttonA.Play();
        Application.Quit();
    }
}

