﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour
{
    [Header("Movement")]
    public float speed;
    private float xAxis, yAxis;
    public Vector2 limits;
    public Vector2 limitesNegativos;

    [Header ("Shooting")]
    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    private float nextFire;


    private float shootTime = 0;

    public Weapon weapon;

    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps2;
    [SerializeField] AudioSource audioExp;
    [SerializeField] AudioSource audioLaser;

    [Header("Player Lives")]
    public GameObject Life_01;
    public GameObject Life_02;
    public GameObject Life_03;
    public int lives = 3;
    private bool iamDead = false;


    // Update is called once per frame
    void Update()
    {
        if (iamDead)
        {
            return;
        }
        shootTime += Time.deltaTime;

        xAxis = Input.GetAxis("Horizontal");
        yAxis = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(xAxis, yAxis);

        transform.Translate(movement * speed * Time.deltaTime);

        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < limitesNegativos.x)
        {
            transform.position = new Vector3(limitesNegativos.x, transform.position.y, transform.position.z);
        }

        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }
        else if (transform.position.y < limitesNegativos.y)
        {
            transform.position = new Vector3(transform.position.x, limitesNegativos.y, transform.position.z);
        }

        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, Quaternion.identity);
            audioLaser.Play();
        }



    }

     public void Shoot()
    {
        if (shootTime > weapon.GetCadencia())
        {
            shootTime = 0f;
            weapon.Shoot();
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy" || other.tag == "EnemyBullet")
        {
            StartCoroutine(DestroyPlayer());
        }
    }

    IEnumerator DestroyPlayer()
    {
        iamDead = true;

        lives--;

        graphics.SetActive(false);

        collider.enabled = false;

        ps2.Play();

        audioExp.Play();

        if (lives == 2)
        {
            Life_03.SetActive(false);
        }
        else if (lives == 1)
        {
            Life_02.SetActive(false);
        }
        else if (lives == 0)
            Life_01.SetActive(false);

        yield return new WaitForSeconds(1.0f);

        if (lives > 0)
        {
            StartCoroutine(InmortalPlayer());
        }if (lives == 0)
            SceneManager.LoadScene("GameOverScreen");

    }

    IEnumerator InmortalPlayer()
    {
        iamDead = false;
        graphics.SetActive(true);


        for (int i = 0; i < 15; i++)
        {
            graphics.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graphics.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        collider.enabled = true;
    }

}