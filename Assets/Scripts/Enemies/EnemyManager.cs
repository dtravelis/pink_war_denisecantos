﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] GameObject[] formaciones;

    private int elegida;

    public float timeLaunchFormacion;

    private float currentTime = 0;

    private float managerY;


    void Update()
    {
        elegida = Random.Range(0, formaciones.Length);

        managerY = Random.Range(-5, 5);
        currentTime += Time.deltaTime;
        if (currentTime > timeLaunchFormacion)
        {
            currentTime = 0;
            Instantiate(formaciones[elegida], new Vector3(this.transform.position.x, managerY, 0), Quaternion.identity, this.transform);
        }
    }


}
