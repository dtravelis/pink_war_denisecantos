﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserMovement : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rig;


    void Awake()
    {
        rig = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        rig.velocity = transform.right * speed;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Limits")
        {
            Destroy(gameObject);
        }
    }

}
